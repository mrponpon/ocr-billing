FROM git-ai.inet-th.net:5555/ponpon/cuda_pytorch_imagebase:cuda11.6.1-runtime
ADD . ai
COPY . .
RUN pip3 install --no-cache-dir -r requirements.txt
CMD uvicorn app.main:app --host "0.0.0.0" --port "5000" --reload --debug
