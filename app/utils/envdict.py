import os

# env variables
config = [
    'ip_api_ocr',
    'manageai_key',
    'GPU',
    'clear_cache_rate'
]
env_config = {k: os.environ.get(k) for k in config}
print(env_config)