from fastapi import FastAPI ,Request
import uvicorn
from starlette.middleware.cors import CORSMiddleware
from app.routes import views
from app.utils.envdict import env_config
import easyocr
import os
import time
import numpy as np
import urllib3
urllib3.disable_warnings()
app = FastAPI()
is_gpu = eval(env_config['GPU'])
print(os.getcwd(),is_gpu)

@app.on_event("startup")
async def startup_event():
    app.reader = easyocr.Reader(['en'], model_storage_directory='app/detection_model/model',recognizer =False,gpu = is_gpu)
    app.reader.detect(np.random.randint(255, size=(100,100,3),dtype=np.uint8))

# Set all CORS enabled origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(views.router)

