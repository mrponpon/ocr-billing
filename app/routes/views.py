from fastapi import APIRouter, Depends, Request
from app.apis.healthcheck.health_check import healthChecking
from app.apis.predict_text.predict_ai import predict_image as predict_text_img
from typing import List, Optional
from pydantic import BaseModel
from starlette.responses import JSONResponse
class Img(BaseModel):
    image_base64: str

class Data(BaseModel):
    data: List[Img]

router = APIRouter()

@router.get("/")
def index():
    return "HELLO OCR API"

@router.get("/health-check")
def view_1(request: Request):
    return JSONResponse(content = healthChecking(request), status_code = 200)

@router.post("/prediction", tags=["api_predict"])
def view_2(request: Request,img_list: Data):
    return JSONResponse(content = predict_text_img(request,img_list), status_code = 200)
