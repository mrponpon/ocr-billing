import json
import os
import base64
import requests
import cv2
import numpy as np
import torch
import gc
import random
from app.utils.logger import log
from app.utils.get_image_box import get_image_list
from app.utils.plot_box import plot_one_box
from app.utils.envdict import env_config
from multiprocessing.pool import ThreadPool
pool = ThreadPool(processes=4)

ip_topsarun_ocr = env_config['ip_api_ocr']
manageai_key = env_config['manageai_key']
clear_cache_rate = int(env_config['clear_cache_rate'])
print(ip_topsarun_ocr,manageai_key,clear_cache_rate)

def ocr_topsarun_batch_api(base64_list):
    headers = {"manageai-key":str(manageai_key)}
    data = {
        "base64img_array": base64_list
    }
    r = requests.post(str(ip_topsarun_ocr), json = data,headers=headers,verify=False)
    print(r)
    status_code = r.status_code
    result = r.json()
    if status_code != 200:
        log.warning(r.raise_for_status())
        print(result)    
    if result !=None:
        return result["prediction"]
    else:
        return ""

def reset(percent=50):
    return random.randrange(100) < percent

def detect_text(img,reader,min_size=20,text_threshold = 0.7,low_text = 0.3,link_threshold = 0.4):
    horizontal_list, free_list = reader.detect(img,canvas_size=1280,min_size=min_size,text_threshold = text_threshold,low_text = low_text,link_threshold=link_threshold)
    image_list = get_image_list(horizontal_list, free_list, img)

    return image_list

def prediction(frame,reader):
    result = []
    base64_list = []
    remove_index = []
    image_list = detect_text(frame, reader)
    if image_list:
        for image in image_list:
            retval, buffer = cv2.imencode('.jpg', image[1])
            img_base64 = base64.b64encode(buffer).decode('utf-8')
            base64_list.append(img_base64)
            box = image[0]
            xyxy = [box[0][0],box[0][1],box[2][0],box[2][1]]
            result.append({
                "class_name": 'text',
                "confidence": 0,
                "result": "",
                "position": {
                    "xmin": int(xyxy[0]),
                    "ymin": int(xyxy[1]),
                    "xmax": int(xyxy[2]),
                    "ymax": int(xyxy[3])
                }
            })
            color =[40, 0, 255]
            plot_one_box(
                xyxy,frame, label='', color=color, line_thickness=2
            )
        # cv2.imwrite('testing/frame.jpg',frame)

        text_list = ocr_topsarun_batch_api(base64_list)

        for i in range(len(result)):
            text = text_list[i]["text"].replace("\n","").replace("\t","").replace("\f","")
            conf = round((text_list[i]["confidence"])*100,2)
            result[i]["result"] = text
            if conf > 1.5:
                result[i]["confidence"] = conf
            else:
                remove_index.append(i)
        n = 0
        for j in remove_index:
            j = j-n
            del result[j]
            n=n+1
    return result

def predict_image(request, img_list):
    reader = request.app.reader
    main_worker = []
    worker_result = []
    for data in img_list.data:
        image_base64 = data.image_base64
        image = base64.b64decode(image_base64)
        npimg = np.frombuffer(image, np.uint8)
        frame = cv2.imdecode(npimg,cv2.IMREAD_COLOR)
        main_worker.append(pool.apply_async(prediction,(frame,reader,)))
        if reset(clear_cache_rate):
            gc.collect()
            torch.cuda.empty_cache()
    for _p in main_worker:
        worker_result.append(_p.get())
    return {
            "message":"AI predict success.",
            "result": worker_result,
            }




